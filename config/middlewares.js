const bodyParser = require('body-parser')
const cors = require('cors')

module.exports = app => {
    app.use(bodyParser.json())
    // parse application/x-www-form-urlencoded

    app.use(cors())
}