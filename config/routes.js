const admin = require('./admin')

module.exports = app => {
    /*
        A forma pardrão de chamar os métodos de
        outros arquivos seria fazer o require
        mas com o consign fica mais simples
    */

    /*
        Cuidado com a ordem das rotas, pode gerar um
        impacto negativo
    */

   app.post('/login',app.api.auth.signIn)
   app.post('/validate',app.api.auth.validateToken)

    app.route('/users')
        .all(app.config.passport.authenticate())
        .post(app.api.user.save)
        .get(admin(app.api.user.get))
        /*
        Aqui estou usando o middlware admin
        para usar esse middleware preciso da autenticação
        do passport pois ele usa o strategy q retorna
        os dados do user
        */

    app.route('/users/save/:id')
        .put(app.api.user.save)

    app.route('/users/user/:id')
        .get(app.api.user.getById)

    app.route('/categories')
        .post(app.api.categorie.save)
        .get(app.api.categorie.get)
    
    app.route('/categories/tree')
        .all(app.config.passport.authenticate())
        .get(app.api.categorie.getTree)

    app.route('/categories/delete/:id')
        .delete(app.api.categorie.remove)

    app.route('/categories/categorie/:id')
        .get(app.api.categorie.getById)


    app.route('/articles')
        .all(app.config.passport.authenticate())
        .post(app.api.article.save)
        .get(app.api.article.get)
        
    app.route('/articles/article/:id')
        .get(app.api.article.get)
    
    app.route('/articles/delete/:id')
        .delete(app.api.article.remove)

    app.route('/articles/categorie/:catId/:page')
        .get(app.api.article.getByCategorie)


    app.route('/stats')
        .get(app.api.model.stat.get)
    
    
       
}