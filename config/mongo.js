const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost/knowledge_stats', {useNewUrlParser: true, useUnifiedTopology:true})
.then(_ => console.log('Conexão com o mongodb funcionando'))
.catch(err => console.log('Conexão com o mongodb não funcionando'))