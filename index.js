const app = require('express')()
const consign = require('consign')
const mongoose = require('mongoose')
require('./config/mongo')
/*
Consign vai facilitar a exportação e importação de módulos
*/

const db = require('./config/db')

app.db = db //Atribuindo o arquivo db
app.mongoose = mongoose
consign()
    .include('./config/passport.js')
    .then('./config/middlewares.js')
    .then('./api/validation.js')
    .then('./api')
    .then('./schedule')
    .then('./config/routes.js')
    .into(app)

app.listen(3005,() => {
    console.log('Server iniciado em http://localhost:3005')
})