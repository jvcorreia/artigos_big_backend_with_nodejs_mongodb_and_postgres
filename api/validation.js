module.exports = app => { 

function existsOrError(value, msg) {
    if (!value) throw msg
    if (Array.isArray(value) && value.length === 0) throw msg
    if (typeof value === 'string' && !value.trim()) throw msg

}

function notExistsOrError(value, msg) {
    if (value) throw msg
    if (Array.isArray(value) && value.length !== 0) throw msg
    if (typeof value === 'string' && value.trim()) throw msg

}

function equalsOrError(value, expectedValue, msg) {
    if (value === expectedValue) {
        return
    } else {
        throw msg
    }
}

function bellowValueOrError(value, expectedValue, msg) {
    if (value.length > expectedValue) {
        throw msg
    }
    return
}

async function userIdExistsOrError(id) {
    const user = 
    await app.db('users')
                .where({id:id})
                .first()

    if (user === undefined) throw 'Usuário informado não existe'
   

}

async function categorieIdExistsOrError(id) {
    const categorie = 
    await app.db('categories')
                .where({id:id})
                .first()

    if (categorie === undefined) throw 'Categoria informada não existe'
   

}

    return {existsOrError, notExistsOrError,
         equalsOrError, bellowValueOrError,
         userIdExistsOrError, categorieIdExistsOrError}
}