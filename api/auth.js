const { authSecret } = require('../.env')
const jwt = require('jwt-simple')
const bcrypt = require('bcrypt-nodejs')

module.exports = app => {
    const signIn = async (req, res) => {
        
        if (!req.body.email || !req.body.password) {
            return res.status(400).send('Informe o usuário e a senha.')
        }

        try {
            const user = await app.db('users')
                .where({ email: req.body.email })
                .first()


            console.log(user)
            if (!user) {
                return res.status(400).send('Email não encontrado')
            }

            if (req.body.password !== user.password) {
                return res.status(401).send('Email/Senha inválida')
            }

            //Vamos pegar a data atual em milissegundos
            const now = Math.floor(Date.now() / 1000)

            const payload = {
                id: user.id,
                email: user.email,
                admin: user.admin,
                iat: now, //issued at
                exp: now + (60 * 60 * 24 * 3)
            }

            res.json({
                ...payload,
                token: jwt.encode(payload, authSecret)
            })
        } catch (error) {
            res.status(500).send(error)
        }

    }

    const validateToken = async (req, res) => {
        const userData = req.body || null

        try {
            if(userData) {
                const token = jwt.decode(userData.token, authSecret)
                if (new Date (token.exp * 1000 > new Date())){
                    return res.send(true)
                }
            }
        } catch (error) {
            //problema com o token
        }

        res.send(false)
    }
    return { signIn, validateToken }
}