module.exports = {
    categorieWithChildren: `
    WITH RECURSIVE subcategories (id) AS (
        SELECT id from categories WHERE id = 1
        UNION ALL
        SELECT c.id FROM subcategories, categories c
            WHERE "parentId" = subcategories.id
    )
    SELECT id FROM subcategories
    `
}