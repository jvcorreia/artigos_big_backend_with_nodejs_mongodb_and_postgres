const queries = require('./queries')
const { v4: uuidv4 } = require('uuid');
module.exports = app => {
    const { existsOrError, bellowValueOrError,
         userIdExistsOrError, categorieIdExistsOrError } = app.api.validation

    const save = async (req, res) => {
        const article = {... req.body}

        if (req.params.id) {
            article.id = req.params.id
        }

        try {
                
            if (!article.id) {
                article.imageUrl = uuidv4();
            }
            
            existsOrError(article.name, 'Nome não informado')
            existsOrError(article.description, 'Descrição não informada')
            existsOrError(article.imageUrl, 'Url da imagem não informada')
            existsOrError(article.content, 'Conteúdo não informado')
            existsOrError(article.userId, 'Usuário não informado')
            existsOrError(article.categorieId, 'Categoria não informada')
            bellowValueOrError(article.description, 1000, 'A descrição deve ter menos de 1000 caracteres')
            bellowValueOrError(article.description, 1000, 'A descrição deve ter menos de 1000 caracteres')
            await userIdExistsOrError(article.userId)
            await categorieIdExistsOrError(article.categorieId)
             
            
        } catch (msg) {
            return res.status(400).send(msg)
        }

        if (article.id) { //update
            app.db('articles')
                .update(article)
                .where({id:article.id})
                .then(article => res.status(200).send('Artigo atualizado com suceso'))
                .catch(err => res.status(500).send(err))

        }else { //insert
            app.db('articles')
            .insert(article)
            .then(article => res.status(200).send('Artigo criado com suceso'))
            .catch(err => res.status(500).send(err))
        }

    }

    const get = (req, res) => {
        app.db('articles')
            .then(articles => res.status(200).send(articles))
            .catch(err => res.status(500).send(err))
    }

    const getById = (req, res) => {
        const id = req.params.id
        app.db('articles')
            .where({id:id})
            .first()
            .then(articles => res.status(200).send(articles))
            .catch(err => res.status(500).send(err))

    }

    const remove = (req, res) => {
        const id = req.params.id
        app.db('articles')
            .where({id:id})
            .del()
            .then(articles => res.status(200).send('Artigo deletado com sucesso'))
            .catch(err => res.status(500).send(err))
    }

    const getByCategorie = async (req, res) => {
        //Nessa função será implementada a paginação para ficar de exemplo
        const catId = req.params.catId
        const page = req.params.page || 1
        const limit = 10
        try {
            const categories = await app.db.raw(queries.categorieWithChildren, catId)
            const ids = categories.rows.map(c => c.id)

        app.db({a: 'articles', b: 'users'})
            .select('a.id', 'a.name', 'a.description', 'a.imageUrl', {author: 'u.name'})
            .limit(limit).offset(page * limit - limit)
            .whereRaw('?? = ??', ['u.id', 'a.userId'])
            .whereIn('categoryId', ids)
            .orderBy('a.id', 'desc')
            .then(articles => res.json(articles))
            .catch(err => res.status(500).send(err))
        } catch (error) {
            res.status(500).send(error)
        }
    }

    
    return {save, get, getById, remove, getByCategorie}
}