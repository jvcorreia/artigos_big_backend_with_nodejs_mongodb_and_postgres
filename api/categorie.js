module.exports = app => {
    const { existsOrError, notExistsOrError } = app.api.validation

    const save = async (req, res) => {
        const categorie = {...req.body}

        if (req.params.id) {
            categorie.id = req.params.id
        }

        try {
            existsOrError(categorie.name, 'Nome da categoria não foi inserido')

            if (categorie.parentId) {
                const parentCategory = await app.db('categories')
                .select('id')
                .where({id:categorie.parentId})
                .first()
                
               
                if (parentCategory == undefined) {
                    throw 'Categoria pai não existe'
                }
            }
                

        } catch (error) {
            return res.status(400).send(error)
        }

        
        if (categorie.id) {
            app.db('categories')
                .update(categorie)
                .where({id:categorie.id})
                .then(categorie => 
                    res.status(200).send('Categoria Atualizada com sucesso'))
                .catch(err => res.status(500).send(err))
        }else{
            app.db('categories')
            .insert(categorie)
            .then(categorie => res.status(201).send(categorie))
            .catch(err => res.status(500).send(err))
        }
 
    }

    const get = (req, res) => {
        app.db('categories')
        .select('id','name','parentId')
        .then(categories => res.json(categories))
        .catch(e => res.status(500).send(e))
    }

    const getById = (req, res) => {
        const id = req.params.id
        app.db('categories')
        .select('id','name','parentId')
        .where({id:id})
        .first()
        .then(categories => res.json(categories))
        .catch(e => res.status(500).send(e))
    }

    const remove = async (req, res) => {
        const id = req.params.id

        try {
            existsOrError(id, 'Código não informado')

            const parentCategory = await app.db('categories')
            .select('id')
            .where({parentId:id})
            .first()
            
            if (parentCategory !== undefined) {
                throw 'Categoria não pode ser deletada pois é pai de outras'
            }

        } catch (error) {
            return res.status(400).send(error)
        }
        
        app.db('categories')
            .where({id:id})
            .del()
            .then(_ => res.status(200).send('Categoria deletada com sucesso'))
            .catch(e => res.status(500).send(e))

    }

    /*

    Função errada, avaliar diferenças

    const toTree = (categories, tree) => {
        if (!tree)  tree = categories.filter(c => !c.parentId)
           
        tree = tree.map(parentNode =>{
                const isChild = node => node.parentId = parentNode.id
                parentNode.children = toTree(categories, categories.filter(isChild))
                return parentNode
            })
        return tree
        
    } */

    //função recebe 2 parâmetros, categories e tree.
    const toTree = (categories, tree) => {
        //Essa primeira linha faz o seguinte:
        //Se o atributo tree passado com parâmetro não tiver setado, ou seja, for undefined
        //Será feito um filtro em todas as categorias passando para tree apenas as que não tem parentID
        //As que não tem parentID são as primeiras. Que não tem pai.
        if(!tree) tree = categories.filter(c => !c.parentId)
        
        //Aqui a variável tree receberá o mapeamento onde para cada elemento será aplicada a função abaixo.
        tree = tree.map(parentNode => {
            //A função abaixo pega um elemento e verifica se ele é filho do do elemento acima. (Parent Node)
            const isChild = node => node.parentId == parentNode.id
            //Então são cria-se o elemento children no parentNode e esse elemento recebe a chamada recursiva
            //da função toTree, Só que agora ao invés de passar TODAS as categorias, será feito um filter
            //que deixará passar apenas as filhas do parentNode.
            parentNode.children = toTree(categories, categories.filter(isChild))
            return parentNode
        })
        return tree
    }


    const getTree = (req, res) => {
        app.db('categories')
            .then(categories => res.json(toTree(categories)))
            .catch(err => res.status(500).send(err))
    }


    return {save, get, remove, getTree, getById}
}