/*
Dentro do module exports, vou implementar uma série de 
função que serão usadas, e vou retornar um objeto com
todas elas
*/

const bcrypt = require('bcrypt-nodejs')


module.exports = app => {


    const encryptPassword =  password => {
        const salt = bcrypt.genSaltSync(10)
        return bcrypt.hashSync(password, salt)
    }

    const save = async (req, res) => {
        const user = {...req.body} //'Clonando' o body da requisição
        console.log(req.body)
        if (req.params.id) {
            user.id = req.params.id
        }

        try{
            app.api.validation.existsOrError(user.name, 'Nome não existe')
            app.api.validation.existsOrError(user.email, 'E-mail não existe')
            app.api.validation.existsOrError(user.password, 'Senha não existe')
            app.api.validation.existsOrError(user.passwordConfirmation, 'Confirmação de senha não existe')
            app.api.validation.equalsOrError(user.password, user.passwordConfirmation, 'Senhas não batem')

            const userFromDb = await app.db('users')
                .where({email: user.email}).first()
            
                if (!user.id) {
                    app.api.validation.notExistsOrError(userFromDb, 'Usuário já cadastrado')
                }
        } catch(msg) {
            return res.status(400).send(msg)
        }
        delete user.passwordConfirmation
        if (user.id) {
            app.db('users')
                .update(user)
                .where({id: user.id})
                .then(user => res.status(200).send(user))
                .catch(err => res.status(500).send(err))
        } else {
            app.db('users')
                .insert(user)
                .then(user => res.status(201).send(user))
                .catch(err => res.status(500).send(err))
        }
    }

    const get = (req, res) => {
        app.db('users')
            .select('id','name','email','admin')
            .then(users => res.json(users))
            .catch(e => res.status(500).send(e))
    }

    const getById = (req, res) => {
        const id = req.params.id

        app.db('users')
        .select('id','name','email','admin')
        .where({id:id})
        .first()
        .then(user => res.status(200).json(user))
        .catch(e => res.status(500).send(e))
    }

    return {save, get, getById}
}